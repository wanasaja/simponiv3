<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-06-18 19:54:35
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-18 20:53:14
*/

use yii\helpers\Url;

if($unreadPesan){
	$unreadPesan = '<a class="unread-pesan" href="'.Url::base().'/pesan/chat">Pesan <span class="badge blink_text" style="margin-left: 8px;">'.$unreadPesan.'</span></a>';
	$unreadPesan .= '<audio autoplay><source src="'.Url::base().'/notif/pesan.ogg"></audio>';
}else{
	$unreadPesan = '<a class="unread-pesan" href="'.Url::base().'/pesan/chat">Pesan</a>';
}

echo $unreadPesan;