<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-04-22 19:09:21
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-07-06 09:33:24
*/

/* @var $this yii\web\View */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;




$this->title = 'Laporan';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
	.form-control {
		padding: 0px 0px;
     	border: 1px solid #cccccc; 
	}

	#loading { display: none; }
	
	/*untuk animasi loading*/
	.loader {
	  border: 16px solid #f3f3f3;
	  border-radius: 50%;
	  border-top: 16px solid #3498db;
	  border-bottom: 16px solid #3498db;
	  width: 120px;
	  height: 120px;
	  -webkit-animation: spin 2s linear infinite;
	  animation: spin 2s linear infinite;
	}

	@-webkit-keyframes spin {
	  0% { -webkit-transform: rotate(0deg); }
	  100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
</style>

<div class="laporan-index">

	<div class="panel panel-default">
	  <div class="panel-body">
		<?php
			$form = ActiveForm::begin([
				'id' => 'lap_form'
			]);
			echo '<label class="control-label">Pilih Rentang Tanggal</label>';
			echo DatePicker::widget([
			    'model' => $model,
			    'attribute' => 'dateAwal',
			    'attribute2' => 'dateAkhir',
			    'options' => ['placeholder' => 'Tanggal Awal'],
			    'options2' => ['placeholder' => 'Tanggal Akhir'],
			    'type' => DatePicker::TYPE_RANGE,
			    'form' => $form,
			    'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
				'layout' => '<span class="input-group-addon">Dari Tanggal</span>{input1}{separator}<span class="input-group-addon">Sampai Tanggal</span>{input2}',
			    'pluginOptions' => [
			        'format' => 'yyyy-mm-dd',
			        'autoclose' => true,
			    ]
			]);
		?>

		<div class="row">
		<div class="form-group" style="padding-top:2%;">
		    <div class="row">
				<div class="col-md-12">
		    	<div class="col-md-1">
		    		<?php 
		        echo Html::submitButton('Proses', [
		            'class' => 'btn btn-primary',
		        ]) ?>
		    	</div>
		    	<div class="col-md-6" style="padding-left:0px;">
		    		<span style="padding-top: 13px; display: block;font-size: 90%;line-height: 1.42857143;color: #999999;">
		    			*<em>Tanpa rentang tanggal akan menampilkan semua rujukan.</em>	
		    		</span>
		    	</div>
		    </div>

		</div>
		    </div>
		</div>
		

		<?php 
			ActiveForm::end();
		?>
	  </div>
	</div>

	
	<div class="table-laporan">
	
	<p>
		<?= Html::a('Export Excel', ['export-excel?dari=&sampai='], ['class'=>'btn btn-info disabled']); ?>
		<?= Html::a('Export Pdf', ['export-pdf?dari=&sampai='], ['class'=>'btn btn-warning disabled']); ?>
	</p>

	<div class="table-responsive">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => 'Pertama',
            'lastPageLabel'  => 'Terakhir'
        ],
        'rowOptions' => function($model) {
            if ($model->status==='Menunggu') return ['class' => 'danger'];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'tgl_masuk',
                'headerOptions' => ['style' => 'width:14%'],
                'value' => function($model){
                    return $model->tgl_masuk;
                    // return Yii::$app->formatter->asDate($model->tgl_masuk);
                },
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => ' Contoh: 2018-12-23'
                 ]
            ],
            'jam_masuk',
            [
                'attribute' => 'asal_rujukan_text',
                'header' => '<font color="#2fa4e">Asal Rujukan</font>',
                'value' => function ($model) {
                    return $model->asal_rujukan_text;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'tujuan_rujukan_text',
                'header' => '<font color="#2fa4e">Tujuan Rujukan</font>',
                'value' => function ($model) {
                    return $model->tujuan_rujukan_text;
                },
                'format' => 'html',
            ],
            'nama',
            'tglahir',
            [
                'attribute' => 'jk',
                'value' => function ($model) {
                    if($model->jk)
                        return $model->jk;
                    else
                        return '';
                },
                'format' => 'html',
            ],
            'diagnosa:ntext',
            'status',
            'info_balik:ntext',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
	</div>
	
	<div class="loader" id="loading"></div>

</div>


<?php

$this->registerJs('
	// $(document).ajaxStart(function() {
	//   	$("#loading").show();
	// }).ajaxStop(function() {
	// 	$("#loading").hide();
	// });

	// $(document).ajaxStart(function() {
	//   	$("#loading").show();
	// });

	$("#lap_form").on("submit", function(e){
		$(".table-laporan").replaceWith("<div class=\"table-laporan\"></div>");

		e.preventDefault();
	    var form = $(this);
	    var formData = form.serialize();
		
	    $.ajax({
	        url: form.attr("action"),
	        type: form.attr("method"),
	        data: formData,
	        beforeSend: function() {
				$("#loading").show();
			},
	        success: function (data) {
	             // console.log(data);
	             $(".table-laporan").replaceWith(data);
	             $("#loading").hide();
	        },
	        error: function () {
	            alert("Something went wrong");
	        }
	    });
	    return false;
	});
');