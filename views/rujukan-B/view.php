<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-05 08:44:52
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-06 11:58:50
*/

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rujukan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rujukans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rujukan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ibu_nama',
            'ibu_tplahir',
            'ibu_tglahir',
            'ibu_umur',
            'ibu_alamat:ntext',
            'ibu_carabayar',
            'bayi_jk',
            'bayi_nama',
            'bayi_tplahir',
            'bayi_tglahir',
            'bayi_umur',
            'bayi_alamat:ntext',
            'bayi_carabayar',
            'asal_rujukan:ntext',
            'tujuan_rujukan',
            'tindakan_ygdiberikan:ntext',
            'alasan_rujukan:ntext',
            'diagnosa:ntext',
            'kesadaran',
            'tekanan_darah',
            'nadi',
            'suhu',
            'pernapasan',
            'berat_badan',
            'tinggi_badan',
            'lila',
            'nyeri',
            'keterangan_lain:ntext',
            'info_balik:ntext',
            'status',
        ],
    ]) ?>

</div>
