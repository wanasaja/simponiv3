<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-06-26 09:57:54
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-26 10:56:13
*/

?>

<div class="tamu-lupa-pass" style="text-align: center; margin:50px">
	<strong><h4 style="color:black; margin-bottom: 0px">PERHATIAN !</h4></strong> <br>
	 <strong>Aplikasi ini tidak boleh di Logout, agar Monitoring dapat terus berjalan.<br/>
	 Apakah anda yakin ingin Logout?</strong>

</div>