<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-11 17:37:35
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-07-06 09:29:28
*/

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\components\Penolong;
use yii\widgets\Pjax;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RujukanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Riwayat Rujukan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rujukan-index">

    <p>
        <?= Html::a(Icon::show('ambulance').' &nbsp;Tambah Rujukan', ['tambah'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table-responsive tabel-riwayat-rujukan">
    <?php Pjax::begin([
        'id' => 'pjax-tb-riw',
        ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => 'Pertama',
            'lastPageLabel'  => 'Terakhir'
        ],
        'rowOptions' => function($model) {
            if ($model->status==='Menunggu') return ['class' => 'warning'];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'tgl_masuk',
                'headerOptions' => ['style' => 'width:14%'],
                'value' => function($model){
                    return $model->tgl_masuk;
                    // return Yii::$app->formatter->asDate($model->tgl_masuk);
                },
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Contoh: 2018-12-23'
                 ]
            ],
            'jam_masuk',
            'nama',
            [
                'attribute' => 'asal_rujukan_text',
                'header' => '<font color="#2fa4e">Asal Rujukan</font>',
                'value' => function ($model) {
                    return $model->asal_rujukan_text;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'tujuan_rujukan_text',
                'header' => '<font color="#2fa4e">Tujuan Rujukan</font>',
                'value' => function ($model) {
                    return $model->tujuan_rujukan_text;
                },
                'format' => 'html',
            ],
            'alasan_rujukan:ntext',
            'diagnosa:ntext',
            'info_balik:ntext',
            // [
            //     'headerOptions' => ['class' => 'text-center'],
            //     'contentOptions' => ['class' => 'text-center'],
            //     'attribute' => 'status',
            //     'value' => function ($model) {
            //         return Penolong::label($model->status);
            //     },
            //     'format' => 'html',
            // ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'header' => 'Status',
                'headerOptions' => ['style' => 'color:#2fa4e7; text-align:center;'],
                'contentOptions' => ['class' => 'text-center'],
                'template' => '{status}{hapus}',
                'visibleButtons'=>[
                    'hapus'=> function($model){
                          return $model->status==='Menunggu';
                     },
                ],
                'buttons'  => [     
                    'status' => function ($url, $model) {
                        return Penolong::label($model->status);
                    },               
                    'hapus' => function ($url, $model) {
                        $url = Url::to(['rujukan/hapus', 'id' => $model->id]);
                        return Html::a('<span class=""><li class="fa fa-trash"></li></span>', $url, [
                            'class' => 'btn btn-danger btn-xs',
                            'style' => 'margin-left: 5px;margin-bottom: 1px;',
                            'data-toggle'=>'tooltip', 
                            'title'=>'Hapus Rujukan',
                            'data' => [
                                'method' => 'post',
                            ],
                        ]);
                    },
                ]
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'header' => 'Aksi',
                'headerOptions' => ['style' => 'color:#2fa4e7; text-align:center;'],
                'contentOptions' => ['class' => 'text-center'],
                'template' => '{cetak}',
                'visibleButtons'=>[
                    'cetak'=> function($model){
                          return $model->status==='Diterima' || $model->status==='Tidak Diterima';
                     },
                ],
                'buttons'  => [                    
                    'cetak' => function ($url, $model) {
                        $url = Url::to(['rujukan/cetak', 'id' => $model->id]);
                        return Html::a('<span class="">Print</span>', $url, [
                            'class' => 'btn btn-info btn-xs',
                            'style' => 'width:100%',
                            // 'target' => '_blank',
                            // 'data-toggle'=>'tooltip', 
                            'title'=>'Akan Mencetak PDF di Tab Baru.',
                            'data' => [
                                'method' => 'post',
                            ],
                        ]);
                    },
                ]
            ],


            // 'jenis',
            // 'jk',
            //'tplahir',
            //'tglahir',
            //'umur',
            //'alamat:ntext',
            //'cara_bayar',
            //'nik',
            //'no_bpjs_jkd',
            //'anamnesa:ntext',
            //'kesadaran',
            //'tekanan_darah',
            //'nadi',
            //'suhu',
            //'pernapasan',
            //'nyeri',
            //'pemeriksaan_fisik:ntext',
            //'pemeriksaan_penunjang:ntext',
            //'tindakan_yg_sdh_diberikan:ntext',
            //'status',

            // ['class' => 'yii\grid\ActionColumn'],

            
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
    
</div>
