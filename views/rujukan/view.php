<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-11 17:37:35
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-07-06 10:16:16
*/

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Penolong;

/* @var $this yii\web\View */
/* @var $model app\models\Rujukan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rujukans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rujukan-view">

    <?= DetailView::widget([
        'model' => $model,
        'template'=>'<tr><th width="30%">{label}</th><td>{value}</td></tr>',
        'attributes' => [
            [
                'attribute' => 'tgl_masuk',
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->tgl_masuk);
                },
            ],
            [
                'attribute' => 'jam_masuk',
                'value' => function($model){
                    return $model->jam_masuk.' WIB';
                },
            ],
            'jenis',
            'nama',
            'jk',
            [
                'attribute' => 'berat_bayi',
                'value' => function($model){
                    return $model->berat_bayi.' gram';
                },
            ],

            'tplahir',
            [
                'attribute' => 'tglahir',
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->tglahir);
                },
            ],
            'umur',
            'alamat:ntext',
            'cara_bayar',
            'nik',
            'no_bpjs_jkd',
            'asal_r',
            'tujuan_r',
            'alasan_rujukan:ntext',
            'anamnesa:ntext',
            'kesadaran',

            [
                'attribute' => 'tekanan_darah',
                'value' => function($model){
                    return $model->tekanan_darah.' mmHg';
                },
            ],
            [
                'attribute' => 'nadi',
                'value' => function($model){
                    return $model->nadi.' x/menit';
                },
            ],
            [
                'attribute' => 'suhu',
                'value' => function($model){
                    return $model->suhu.' C';
                },
            ],
            [
                'attribute' => 'pernapasan',
                'value' => function($model){
                    return $model->pernapasan.' x/menit';
                },
            ],

            'nyeri',
            'pemeriksaan_fisik:ntext',
            'pemeriksaan_penunjang:ntext',
            'diagnosa:ntext',
            'tindakan_yg_sdh_diberikan:ntext',
            'info_balik:ntext',
            [
                'attribute' => 'status',
                'value' => function($model){
                    return Penolong::label($model->status);
                },
                'format' => 'html'
            ]
        ],
    ]) ?>

</div>
