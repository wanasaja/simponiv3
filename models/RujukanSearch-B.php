<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-05 08:44:52
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-06 11:32:18
*/

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rujukan;

/**
 * RujukanSearch represents the model behind the search form of `app\models\Rujukan`.
 */
class RujukanSearch extends Rujukan
{
    /**
     * @inheritdoc
     */
    public $asal_rujukan_text;
    public $tujuan_rujukan_text;

    public function rules()
    {
        return [
            [['id', 'berat_badan', 'tinggi_badan'], 'integer'],
            [['tgl_masuk', 'ibu_nama', 'ibu_tplahir', 'ibu_tglahir', 'ibu_umur', 'ibu_alamat', 'ibu_carabayar', 'bayi_jk', 'bayi_nama', 'bayi_tplahir', 'bayi_tglahir', 'bayi_umur', 'bayi_alamat', 'bayi_carabayar', 'asal_rujukan', 'tujuan_rujukan', 'tindakan_ygdiberikan', 'alasan_rujukan', 'diagnosa', 'kesadaran', 'tekanan_darah', 'nadi', 'pernapasan', 'lila', 'nyeri', 'keterangan_lain', 'info_balik', 'status'], 'safe'],
            [['asal_rujukan_text', 'tujuan_rujukan_text'], 'safe'],
            [['suhu'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $idLogin)
    {
        $urutBy[] = new \yii\db\Expression('status = "Menunggu" desc');
        $urutBy[] = new \yii\db\Expression('id desc');

        $query = Rujukan::find()
                ->where(['=', 'asal_rujukan', $idLogin])
                ->orderBy($urutBy);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'tgl_masuk' => $this->tgl_masuk,
            'ibu_tglahir' => $this->ibu_tglahir,
            'bayi_tglahir' => $this->bayi_tglahir,
            'suhu' => $this->suhu,
            'berat_badan' => $this->berat_badan,
            'tinggi_badan' => $this->tinggi_badan,
        ]);

        $query->andFilterWhere(['like', 'ibu_nama', $this->ibu_nama])
            ->andFilterWhere(['like', 'tgl_masuk', $this->tgl_masuk])
            ->andFilterWhere(['like', 'ibu_tplahir', $this->ibu_tplahir])
            ->andFilterWhere(['like', 'ibu_umur', $this->ibu_umur])
            ->andFilterWhere(['like', 'ibu_alamat', $this->ibu_alamat])
            ->andFilterWhere(['like', 'ibu_carabayar', $this->ibu_carabayar])
            ->andFilterWhere(['like', 'bayi_jk', $this->bayi_jk])
            ->andFilterWhere(['like', 'bayi_nama', $this->bayi_nama])
            ->andFilterWhere(['like', 'bayi_tplahir', $this->bayi_tplahir])
            ->andFilterWhere(['like', 'bayi_umur', $this->bayi_umur])
            ->andFilterWhere(['like', 'bayi_alamat', $this->bayi_alamat])
            ->andFilterWhere(['like', 'bayi_carabayar', $this->bayi_carabayar])
            // ->andFilterWhere(['like', 'asal_rujukan', $this->asal_rujukan])
            // ->andFilterWhere(['like', 'tujuan_rujukan', $this->tujuan_rujukan])
            ->andFilterWhere(['like', 'tindakan_ygdiberikan', $this->tindakan_ygdiberikan])
            ->andFilterWhere(['like', 'alasan_rujukan', $this->alasan_rujukan])
            ->andFilterWhere(['like', 'diagnosa', $this->diagnosa])
            ->andFilterWhere(['like', 'kesadaran', $this->kesadaran])
            ->andFilterWhere(['like', 'tekanan_darah', $this->tekanan_darah])
            ->andFilterWhere(['like', 'nadi', $this->nadi])
            ->andFilterWhere(['like', 'pernapasan', $this->pernapasan])
            ->andFilterWhere(['like', 'lila', $this->lila])
            ->andFilterWhere(['like', 'nyeri', $this->nyeri])
            ->andFilterWhere(['like', 'keterangan_lain', $this->keterangan_lain])
            ->andFilterWhere(['like', 'info_balik', $this->info_balik])
            ->andFilterWhere(['like', 'status', $this->status]);

        if($this->asal_rujukan_text!=null)
        {
            $query->andFilterWhere(['IN', 'asal_rujukan', 
                (new \yii\db\Query())->select('id')
                    ->from('pengguna')
                    ->where(['like', 'nama_rs_puskesmas', $this->asal_rujukan_text])]);
        }
        if($this->tujuan_rujukan_text!=null)
        {
            $query->andFilterWhere(['IN', 'tujuan_rujukan', 
                (new \yii\db\Query())->select('id')
                    ->from('pengguna')
                    ->where(['like', 'nama_rs_puskesmas', $this->tujuan_rujukan_text])]);
        }

        return $dataProvider;
    }

    public function searchMonitoring($params)
    {
        $urutBy[] = new \yii\db\Expression('status = "Menunggu" desc');
        $urutBy[] = new \yii\db\Expression('id desc');

        $query = Rujukan::find()
                ->orderBy($urutBy);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'tgl_masuk' => $this->tgl_masuk,
            'ibu_tglahir' => $this->ibu_tglahir,
            'bayi_tglahir' => $this->bayi_tglahir,
            'suhu' => $this->suhu,
            'berat_badan' => $this->berat_badan,
            'tinggi_badan' => $this->tinggi_badan,
        ]);

        $query->andFilterWhere(['like', 'ibu_nama', $this->ibu_nama])
            ->andFilterWhere(['like', 'tgl_masuk', $this->tgl_masuk])
            ->andFilterWhere(['like', 'ibu_tplahir', $this->ibu_tplahir])
            ->andFilterWhere(['like', 'ibu_umur', $this->ibu_umur])
            ->andFilterWhere(['like', 'ibu_alamat', $this->ibu_alamat])
            ->andFilterWhere(['like', 'ibu_carabayar', $this->ibu_carabayar])
            ->andFilterWhere(['like', 'bayi_jk', $this->bayi_jk])
            ->andFilterWhere(['like', 'bayi_nama', $this->bayi_nama])
            ->andFilterWhere(['like', 'bayi_tplahir', $this->bayi_tplahir])
            ->andFilterWhere(['like', 'bayi_umur', $this->bayi_umur])
            ->andFilterWhere(['like', 'bayi_alamat', $this->bayi_alamat])
            ->andFilterWhere(['like', 'bayi_carabayar', $this->bayi_carabayar])
            // ->andFilterWhere(['like', 'asal_rujukan', $this->asal_rujukan])
            // ->andFilterWhere(['like', 'tujuan_rujukan', $this->tujuan_rujukan])
            ->andFilterWhere(['like', 'tindakan_ygdiberikan', $this->tindakan_ygdiberikan])
            ->andFilterWhere(['like', 'alasan_rujukan', $this->alasan_rujukan])
            ->andFilterWhere(['like', 'diagnosa', $this->diagnosa])
            ->andFilterWhere(['like', 'kesadaran', $this->kesadaran])
            ->andFilterWhere(['like', 'tekanan_darah', $this->tekanan_darah])
            ->andFilterWhere(['like', 'nadi', $this->nadi])
            ->andFilterWhere(['like', 'pernapasan', $this->pernapasan])
            ->andFilterWhere(['like', 'lila', $this->lila])
            ->andFilterWhere(['like', 'nyeri', $this->nyeri])
            ->andFilterWhere(['like', 'keterangan_lain', $this->keterangan_lain])
            ->andFilterWhere(['like', 'info_balik', $this->info_balik])
            ->andFilterWhere(['like', 'status', $this->status]);

        if($this->asal_rujukan_text!=null)
        {
            $query->andFilterWhere(['IN', 'asal_rujukan', 
                (new \yii\db\Query())->select('id')
                    ->from('pengguna')
                    ->where(['like', 'nama_rs_puskesmas', $this->asal_rujukan_text])]);
        }
        if($this->tujuan_rujukan_text!=null)
        {
            $query->andFilterWhere(['IN', 'tujuan_rujukan', 
                (new \yii\db\Query())->select('id')
                    ->from('pengguna')
                    ->where(['like', 'nama_rs_puskesmas', $this->tujuan_rujukan_text])]);
        }

        return $dataProvider;
    }

}
