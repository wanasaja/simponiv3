<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aku".
 *
 * @property int $id
 * @property string $pukul
 */
class Aku extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aku';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pukul'], 'required'],
            [['pukul'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pukul' => 'Pukul',
        ];
    }
}
