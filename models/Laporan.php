<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-22 05:33:40
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-22 11:28:43
*/

namespace app\models;

use yii\base\Model;


class Laporan extends Model 
{
	public $dateAwal;
    public $dateAkhir;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // [['dateAwal', 'dateAkhir'], 'required', 'message' => '{attribute} tidak boleh kosong.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'dateAwal' => 'Tanggal Awal',
            'dateAkhir' => 'Tanggal Akhir',
        ];
    }
}