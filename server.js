const http = require('http')
const socket = require('socket.io')

const server = http.createServer()
const io = socket.listen(server)
const port = 3000

server.listen(port, '0.0.0.0');

io.on('connection', function(socket) {

    socket.on('tambah_rujukan', function() {
        io.emit('replace_monitoring')
        io.emit('replace_unread_monitoring')
    })

    socket.on('proses_rujukan', function() {
        io.emit('replace_riwayat_rujukan')
        io.emit('replace_monitoring')
    })

    socket.on('hapus_rujukan', function() {
        io.emit('replace_monitoring')
        io.emit('replace_unread_monitoring')
    });

    socket.on('chat_kirim', function() {
        // io.emit('replace_unread_pesan')
        io.emit('chat_update')
    });

    // socket.on('chat_baca', function() {
    //     io.emit('replace_unread_pesan')
    // });
})
